import { TestBed } from '@angular/core/testing';

import { GoodsItemService } from './goods-item.service';

describe('GoodsItemService', () => {
  let service: GoodsItemService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GoodsItemService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
