import { Injectable } from '@angular/core';
import {GoodsCaregoryRestService} from "../rest/goods-caregory-rest.service";
import {Observable} from "rxjs";
import {IMenuCategoryInfo} from "../../models/categories/main-menu-categories";

@Injectable({
  providedIn: 'root'
})
export class GoodsCategoryService {

  constructor(private goodsCategoryRestService: GoodsCaregoryRestService) { }

  getGoodsCategories():Observable<IMenuCategoryInfo[]> {
    return this.goodsCategoryRestService.getGoodsCategories();
  }

}
