import {Injectable} from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class GoodsItemPageService {

  constructor() {
    }

    getGoodsItemRequestObj(id:string|undefined,catUrl: string|null, subcatUrl:string|null){
    if (id) {
      return {
         category: catUrl,
         subcategory: subcatUrl,
         id: id
       };
    } else {
       return{
            category: catUrl,
            subcategory: subcatUrl,
            id: localStorage.getItem('goodsItemId')
          }
   }
}
}
