import { TestBed } from '@angular/core/testing';

import { GoodsItemPageService } from './goods-item-page.service';

describe('GoodsItemPageService', () => {
  let service: GoodsItemPageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GoodsItemPageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
