import { Injectable } from '@angular/core';
import {GoodsListRestService} from "../rest/goods-list-rest.service";
import {Observable} from "rxjs";
import {IGoodsCard} from "../../models/goods/goods-card";
import {IGoodsItemCard} from "../../models/goods/goods-item-card";

@Injectable({
  providedIn: 'root'
})
export class GoodsListService {

  constructor(private goodsListRestService: GoodsListRestService) { }

  getPapercratGoodsList(): Observable<IGoodsCard[]>{
    return this.goodsListRestService.getPapercratGoodsList();
  }

  testRequest(category:any, subcategory:any):Observable<IGoodsItemCard[]>{
    return this.goodsListRestService.testRequest(category,subcategory);
  }

}
