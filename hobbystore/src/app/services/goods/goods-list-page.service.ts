import { Injectable } from '@angular/core';
import {IGoodsItemCard} from "../../models/goods/goods-item-card";
import {IFilterOption} from "../../models/aside-filter/aside-filter-option";
import {Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class GoodsListPageService {

 private filterOptionSubject = new Subject<IFilterOption[]>();
 readonly filterOption$ = this.filterOptionSubject.asObservable();

 private navOptionSubject = new Subject<boolean>();
 readonly navOption$ = this.navOptionSubject.asObservable();

 private modalCloseSubject = new Subject<boolean>();
 readonly modalClosed$ = this.modalCloseSubject.asObservable();

  constructor() { }

  countGoodsTotalRating(goodsArr:IGoodsItemCard[]){
    return goodsArr.map((item)=>{
      item.total_rating = Math.round(
        (item.rating1 + 2*item.rating2 + 3*item.rating3 + 4*item.rating4 + 5*item.rating5)/(item.rating1+item.rating2 + item.rating3 + item.rating4 + item.rating5));
      console.log(item.total_rating);
      return item});
  }

  sortGoodsByLowPrice(data:IGoodsItemCard[],key: string): IGoodsItemCard[]{
   const sortedOrderArr = [...data];

   //сортировка по возрастанию цены
   if(key === "LP"){
     sortedOrderArr.sort((item1, item2) => {
       if (item1.price - item2.price >= 0) {
         return 1;
       } else {
         return -1;
       }
     });

     //сортировка по убыванию цены
   } else if(key === "HP"){
     sortedOrderArr.sort((item1, item2) => {
       if (item1.price - item2.price >= 0) {
         return -1;
       } else {
         return 1;
       }
     });
  } else if (key === "Rate"){
     // @ts-ignore
     sortedOrderArr.sort((item1, item2) => {
       if(item1.total_rating && item2.total_rating){
         if (item1.total_rating - item2.total_rating >= 0) {
           return -1;
         } else {
           return 1;
         }
       }
    });
   }
    return sortedOrderArr;
 }

 getAsideFilterOption(option:IFilterOption[]): void{
  //  console.log('получили пункт меню в сервис ', option);
    this.filterOptionSubject.next(option);
 }

 getNavOptionChangeState(state:boolean):void{
    console.log('вывели изменение пункта меню ', state);
    this.navOptionSubject.next(state);
 }

 getModalVisibility(state:boolean):void{
    console.log('вывели изменение видимости модал. окна ', state);
    this.modalCloseSubject.next(state);
 }

  }
