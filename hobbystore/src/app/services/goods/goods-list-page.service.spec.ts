import { TestBed } from '@angular/core/testing';

import { GoodsListPageService } from './goods-list-page.service';

describe('GoodsListPageService', () => {
  let service: GoodsListPageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GoodsListPageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
