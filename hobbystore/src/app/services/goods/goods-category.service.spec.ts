import { TestBed } from '@angular/core/testing';

import { GoodsCategoryService } from './goods-category.service';

describe('GoodsCategoryService', () => {
  let service: GoodsCategoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GoodsCategoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
