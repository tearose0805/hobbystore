import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {IGoodsItemCard} from "../../models/goods/goods-item-card";
import {GoodItemRestService} from "../rest/good-item-rest.service";
import {IGoodsItemStock} from "../../models/goods/goods-item-stock";

@Injectable({
  providedIn: 'root'
})
export class GoodsItemService {

  constructor(private goodItemRestService: GoodItemRestService) { }

  getGoodsItemByID(obj: any): Observable<IGoodsItemCard>{
    return this.goodItemRestService.getGoodsItemByID(obj);
  }

  getGoodsStockByItemID(obj: any): Observable<IGoodsItemStock>{
    return this.goodItemRestService.getGoodsStockByItemID(obj);
  }

  setGoodsStockReserve(obj: any, body:any): Observable<IGoodsItemStock>{
    return this.goodItemRestService.setGoodsStockReserve(obj,body);
  }

  setGoodsItemRating(obj:any,body:any):Observable<IGoodsItemCard>{
    return  this.goodItemRestService.setGoodsItemRating(obj, body);
  }
}
