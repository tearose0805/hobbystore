import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {ICart} from "../../models/cart/cart-item";
import {CartRestService} from "../rest/cart-rest.service";

@Injectable({
  providedIn: 'root'
})
export class CartService {

  uniqueID: string = '';

  constructor(private cartRestService: CartRestService) { }

  generateClientUID(): string{
    this.uniqueID = crypto.randomUUID();
    console.log('сформировали уникальный id ', this.uniqueID);
    localStorage.setItem('UID',this.uniqueID);
    return this.uniqueID;
    }

    findGoodsItem(arr:any, product:any){
      const itemObj = arr.find((item: any)=> item.itemID === product.itemID);
      return {...itemObj};
    }

    findGoodsItemIndex(arr:any, product:any): number{
      return arr.findIndex((item:any)=>item.itemID === product.itemID);
    }

  createCartforUID(body:any): Observable<ICart>{
    return this. cartRestService.createCartforUID(body);
  }

  addGoodsItemToCart(UID:string|null, body:any): Observable<ICart>{
    return this.cartRestService.addGoodsItemToCart(UID,body);
  }

  getCartByUID(UID: string): Observable<ICart>{
    return this.cartRestService.getCartByUID(UID);
  }
}
