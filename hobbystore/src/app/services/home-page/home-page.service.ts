import {Injectable} from '@angular/core';
import {HomePageRestService} from "../rest/home-page-rest.service";
import {Observable, Subject} from "rxjs";
import {IOffer} from "../../models/offers/home-page-offers";
import {IGoodsCard} from "../../models/goods/goods-card";
import {IMenuCategory} from "../../models/categories/main-menu-categories";


@Injectable({
  providedIn: 'root'
})
export class HomePageService {
  //private categorySubject = new Subject<IMenuCategory>();
 // readonly goodsCategoryObs$ = this.categorySubject.asObservable();

  goodsCategory: any = {};

  constructor(private homePageRestService: HomePageRestService) { }

  getCarouselOffers():Observable<IOffer[]> {
    return this.homePageRestService.getCarouselOffers();
  }
  getCarouselBestChoices(): Observable<IGoodsCard[]>{
    return this.homePageRestService.getCarouselBestChoices();
  }


}
