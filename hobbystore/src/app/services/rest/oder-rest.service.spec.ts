import { TestBed } from '@angular/core/testing';

import { OderRestService } from './oder-rest.service';

describe('OderRestService', () => {
  let service: OderRestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OderRestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
