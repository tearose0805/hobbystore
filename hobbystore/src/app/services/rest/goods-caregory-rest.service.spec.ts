import { TestBed } from '@angular/core/testing';

import { GoodsCaregoryRestService } from './goods-caregory-rest.service';

describe('GoodsCaregoryRestService', () => {
  let service: GoodsCaregoryRestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GoodsCaregoryRestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
