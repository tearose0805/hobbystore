import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { Observable, throwError } from 'rxjs';
import {ISearchCategories} from "../../models/categories/search-categories";
import {IGoodsCategories} from "../../models/categories/goods-categories";
import {IGoodsItemCard} from "../../models/goods/goods-item-card";


@Injectable({
  providedIn: 'root'
})
export class SearchCategoriesRestService {

  constructor(private http: HttpClient) { }

  getSearchCategories():Observable<ISearchCategories[]>{
    return this.http.get<ISearchCategories[]>('/assets/mocks/categories/search-categories.json');
  }

  getGoodsCategories():Observable<IGoodsCategories[]>{
    return this.http.get<IGoodsCategories[]>('/assets/mocks/categories/goods-categories.json');
  }
  searchGoods(obj:any):Observable<IGoodsItemCard[]>{
    return this.http.get<IGoodsItemCard[]>('http://localhost:3000/' + obj.category +'/'+ obj.subcategory +'/search/' + obj.searchOption);
  }
}

