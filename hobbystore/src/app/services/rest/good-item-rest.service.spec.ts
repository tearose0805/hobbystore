import { TestBed } from '@angular/core/testing';

import { GoodItemRestService } from './good-item-rest.service';

describe('GoodItemRestService', () => {
  let service: GoodItemRestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GoodItemRestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
