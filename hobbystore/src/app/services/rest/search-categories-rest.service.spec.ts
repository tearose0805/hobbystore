import { TestBed } from '@angular/core/testing';

import { SearchCategoriesRestService } from './search-categories-rest.service';

describe('SearchCategoriesRestService', () => {
  let service: SearchCategoriesRestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SearchCategoriesRestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
