import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {IOffer} from "../../models/offers/home-page-offers";
import {Observable} from "rxjs";
import {IGoodsCard} from "../../models/goods/goods-card";

@Injectable({
  providedIn: 'root'
})
export class HomePageRestService {

  constructor(private http: HttpClient) { }

  getCarouselOffers():Observable<IOffer[]>{
    return this.http.get<IOffer[]>('/assets/mocks/home-page-offers/home-carousel-offers.json');
  }

  getCarouselBestChoices(): Observable<IGoodsCard[]>{
    return this.http.get<IGoodsCard[]>('/assets/mocks/home-page-offers/home-best-choices.json');
  }

}
