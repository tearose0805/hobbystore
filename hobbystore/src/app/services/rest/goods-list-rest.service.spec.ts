import { TestBed } from '@angular/core/testing';

import { GoodsListRestService } from './goods-list-rest.service';

describe('GoodsListRestService', () => {
  let service: GoodsListRestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GoodsListRestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
