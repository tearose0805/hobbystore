import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {IMenuCategoryInfo} from "../../models/categories/main-menu-categories";
import {IGoodsCard} from "../../models/goods/goods-card";

@Injectable({
  providedIn: 'root'
})
export class GoodsCaregoryRestService {

  constructor(private http: HttpClient) { }

  getGoodsCategories():Observable<IMenuCategoryInfo[]>{
    return this.http.get<IMenuCategoryInfo[]>('/assets/mocks/categories/goods-categories-info.json');
  }
}
