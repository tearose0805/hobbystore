import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {IOrder} from "../../models/order";

@Injectable({
  providedIn: 'root'
})
export class OderRestService {

  constructor(private http: HttpClient) { }

  createOrder(body:any): Observable<IOrder>{
    return this.http.post<IOrder>('http://localhost:3000/order',body,{headers:{}});
  }
}
