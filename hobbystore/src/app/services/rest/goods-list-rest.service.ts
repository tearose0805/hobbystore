import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {IGoodsCard} from "../../models/goods/goods-card";
import {IGoodsItemCard} from "../../models/goods/goods-item-card";

@Injectable({
  providedIn: 'root'
})
export class GoodsListRestService {

  constructor(private http: HttpClient) { }

  getPapercratGoodsList(): Observable<IGoodsCard[]>{
    return this.http.get<IGoodsCard[]>('/assets/mocks/home-page-offers/home-best-choices.json');
  }

   testRequest(category: any , subcategory: any): Observable<IGoodsItemCard[]> {
    return this.http.get<IGoodsItemCard[]>('http://localhost:3000/' + category +'/'+ subcategory);
     }

  }
