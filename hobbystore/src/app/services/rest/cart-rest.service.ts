import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ICart} from "../../models/cart/cart-item";

@Injectable({
  providedIn: 'root'
})
export class CartRestService {

  constructor(private http: HttpClient) { }

  createCartforUID(body:any): Observable<ICart>{
    return this.http.post<ICart>('http://localhost:3000/cart',body,{headers:{}});
  }
  addGoodsItemToCart(UID:string|null,body:any): Observable<ICart>{
    return this.http.put<ICart>('http://localhost:3000/cart/' + UID, body,{headers:{}});
  }
  getCartByUID(UID: string): Observable<ICart>{
    return this.http.get<ICart>('http://localhost:3000/cart/' + UID,);
  }

  }
