import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {IGoodsItemCard} from "../../models/goods/goods-item-card";
import {IGoodsItemStock} from "../../models/goods/goods-item-stock";

@Injectable({
  providedIn: 'root'
})
export class GoodItemRestService {

  constructor(private http: HttpClient) { }

  getGoodsItemByID(obj: any): Observable<IGoodsItemCard>{
    return this.http.get<IGoodsItemCard>('http://localhost:3000/' + obj.category +'/'+ obj.subcategory + '/' +obj.id);
  }

  getGoodsStockByItemID(obj: any): Observable<IGoodsItemStock>{
    return this.http.get<IGoodsItemStock>('http://localhost:3000/' + obj.subcategory + '/stock/' + obj.id);
     }
  setGoodsStockReserve(obj: any, body: any): Observable<IGoodsItemStock>{
    return this.http.put<IGoodsItemStock>('http://localhost:3000/' + obj.subcategory + '/stock/', body);
  }
 setGoodsItemRating(obj:any,body:any):Observable<IGoodsItemCard>{
    return this.http.put<IGoodsItemCard>('http://localhost:3000/' + obj.category +'/'+ obj.subcategory + '/' +obj.id, body, {headers:{}});
  }


  }
