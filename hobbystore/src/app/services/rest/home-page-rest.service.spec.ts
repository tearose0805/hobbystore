import { TestBed } from '@angular/core/testing';

import { HomePageRestService } from './home-page-rest.service';

describe('HomePageRestService', () => {
  let service: HomePageRestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HomePageRestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
