import { Injectable } from '@angular/core';
import {ICategories} from "../../models/categories/categories";

@Injectable({
  providedIn: 'root'
})
export class SearchModalService {
  sortedCategories: ICategories[]|undefined;
  popularCategories:ICategories[]|undefined;

  constructor() {
  }

  sortCategories(data: ICategories[]): ICategories[] {
     this.sortedCategories = [...data];
     this.sortedCategories.sort((item1, item2) => {
        if (item1.rating - item2.rating >= 0) {
          return -1;
        } else {
          return 1;
        }
      });
    return this.sortedCategories;
    }

  getPopularCategories(data:ICategories[],countItems:number):ICategories[]{
      this.popularCategories = [...data];
      this.popularCategories= this.popularCategories.splice(0,countItems);
      return this.popularCategories;
    }



}
