import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import {SearchCategoriesRestService} from "../rest/search-categories-rest.service";
import {ISearchCategories} from "src/app/models/categories/search-categories"
import {IGoodsCategories} from "../../models/categories/goods-categories";
import {IGoodsItemCard} from "../../models/goods/goods-item-card";

@Injectable({
  providedIn: 'root'
})
export class SearchCategoriesService {

  constructor(private searchCategoriesRest: SearchCategoriesRestService) { }

  getSearchCategories():Observable<ISearchCategories[]>{
    return this.searchCategoriesRest.getSearchCategories();
  }

  getGoodsCategories():Observable<IGoodsCategories[]>{
    return this.searchCategoriesRest.getGoodsCategories();
  }

  searchGoods(obj:any):Observable<IGoodsItemCard[]> {
    return this.searchCategoriesRest.searchGoods(obj);
  }
}
