import { Injectable } from '@angular/core';
import {OderRestService} from "../rest/oder-rest.service";
import {Observable} from "rxjs";
import {IOrder} from "../../models/order";

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private oderRestService: OderRestService) { }

  createOrder(body:any): Observable<IOrder>{
    return this.oderRestService.createOrder(body);
  }
}
