import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class OrderPageService {

  order: any;

  constructor() { }

  saveOderInfo(orderObj:any){
    this.order ={... orderObj};
    console.log('вывели заказ из сервиса ', this.order);
    const serialArr = JSON.stringify(this.order);
    localStorage.setItem('orderObj',serialArr);
  }

  getOderInfo(){
    if (this.order){
      return this.order;
    } else{
      const orderFromLocalStorage = localStorage.getItem('orderObj');
      if(orderFromLocalStorage){
        this.order = JSON.parse(orderFromLocalStorage);
      }
        return this.order;
    }
  }

  deleleOrder(){
    this.order = {};
    localStorage.removeItem('orderObj');
  }
}
