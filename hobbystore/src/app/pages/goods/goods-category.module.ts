import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {GoodsCategoryComponent} from "./goods-category.component";
import {GoodsCategoryRoutingModule} from "./goods-category-routing.module";
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { DropdownModule } from 'primeng/dropdown';
import {ReactiveFormsModule} from "@angular/forms";
import { AsideFilterComponent } from './aside-filter/aside-filter.component';
import { GoodsListComponent } from './goods-list/goods-list.component';
import { RatingModule } from 'primeng/rating';
import {FormsModule} from "@angular/forms";
import { CheckboxModule } from 'primeng/checkbox';

@NgModule({
  declarations: [
    GoodsCategoryComponent,
    AsideFilterComponent,
    GoodsListComponent
  ],
  imports: [
    CommonModule,
    BreadcrumbModule,
    DropdownModule,
    ReactiveFormsModule,
    RatingModule,
    FormsModule,
    CheckboxModule,
    GoodsCategoryRoutingModule,

  ]
})
export class GoodsCategoryModule { }
