import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodsCategoryComponent } from './goods-category.component';

describe('GoodsCategoryComponent', () => {
  let component: GoodsCategoryComponent;
  let fixture: ComponentFixture<GoodsCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GoodsCategoryComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GoodsCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
