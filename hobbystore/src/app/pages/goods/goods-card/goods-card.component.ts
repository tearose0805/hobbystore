import {AfterViewInit, Component, OnInit} from '@angular/core';
import {MenuItem} from "primeng/api";
import {GoodsListPageService} from "../../../services/goods/goods-list-page.service";
import {GoodsItemService} from "../../../services/goods/goods-item.service";
import {IGoodsItemCard} from "../../../models/goods/goods-item-card";
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {GoodsItemPageService} from "../../../services/goods/goods-item-page.service";
import { MessageService } from 'primeng/api';
import { FormControl } from '@angular/forms';
import {IGoodsItemStock} from "../../../models/goods/goods-item-stock";
import {CartService} from "../../../services/cart/cart.service";
import {GoodsCategoryService} from "../../../services/goods/goods-category.service";
import {IMenuCategoryInfo} from "../../../models/categories/main-menu-categories";
import {forkJoin} from "rxjs";


@Component({
  selector: 'app-goods-card',
  templateUrl: './goods-card.component.html',
  styleUrls: ['./goods-card.component.scss']
})
export class GoodsCardComponent implements OnInit, AfterViewInit {
  items: MenuItem[] = [];
  home: MenuItem = {};
  goodsItemIdObj: { [p: string]: any } | undefined;
  goodsItemId: any;
  categoryUrl: string | null = '';
  subcategoryUrl: string | null = '';
  goodsItemObj: IGoodsItemCard | undefined;
  goodsRequestObj: any;
  images: { itemImageSrc: string; thumbnailImageSrc: string; }[] = [];
  displayBasic: boolean = false;
  position: string = 'left';
  goodsList: IGoodsItemCard[]=[];
  setRating: number = 0;
  fragment:string|null = "";
  quantity = new FormControl(1);
  minusButtonState: boolean = true;
  plusButtonState: boolean = false;
  goodsItemStock: IGoodsItemStock | undefined;
  cartUID: string|null = '';
  goodsCategoriesCollection:IMenuCategoryInfo[] = [];
  menuCategoryObj: any;



  constructor(private goodsItemService: GoodsItemService,
              private goodsListPageService: GoodsListPageService,
              private goodsItemPageService: GoodsItemPageService,
              private cartService: CartService,
              private goodsCategoryService: GoodsCategoryService,
              private messageService: MessageService,
              private router: Router,
              private route: ActivatedRoute) {
    this.goodsItemIdObj = this.router.getCurrentNavigation()?.extras.state;
    console.log('получили id из state ', this.goodsItemIdObj);
    if (this.goodsItemIdObj) {
    /*  console.log('еще раз вывести id ', this.goodsItemIdObj);
      const obj: {id: string} = this.goodsItemIdObj[0];
      console.log('сохранили объект из массиса ', obj);
      this.goodsItemId = obj.id || this.goodsItemId?.['id']*/
      this.goodsItemId = this.goodsItemIdObj?.['id']
      console.log('id товара ', this.goodsItemId);
      localStorage.setItem('goodsItemId', this.goodsItemId)
    }

  }


  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.categoryUrl = params.get('category');
      this.subcategoryUrl = params.get('subcategory');
      console.log('категория : ', this.categoryUrl);
      console.log('подкатегория :', this.subcategoryUrl);
    });

    this.route.fragment.subscribe((data) => {
      this.fragment = data;
      console.log('получили фрагмент ', this.fragment);
    });

    this.goodsCategoryService.getGoodsCategories().subscribe((data)=> {
      this.goodsCategoriesCollection = data;
      console.log("получили массив пунктов меню ", this.goodsCategoriesCollection);
    });
/*        if(this.subcategoryUrl ==='summer'){
        this.menuCategoryObj = this.goodsCategoriesCollection.find ((item) =>item.subcategoryUrl===this.subcategoryUrl);
        console.log('нашли объект пункта меню ', this.menuCategoryObj);
    }
    }*/

  this.goodsRequestObj = this.goodsItemPageService.getGoodsItemRequestObj(this.goodsItemId, this.categoryUrl,this.subcategoryUrl);
    console.log('вывести объект запроса на сервер ', this.goodsRequestObj);

    if(this.subcategoryUrl ==='summer'){
      forkJoin([this.goodsItemService.getGoodsStockByItemID({
        category:'papercraft',
        subcategory: 'paper',
        id: this.goodsRequestObj.id}),
        this.goodsItemService.getGoodsStockByItemID({
          category:'art',
          subcategory: 'coloring',
          id: this.goodsRequestObj.id})]).subscribe((data)=>{
            console.log('получили данные по  стоку товара от forkjoin ', data);
      })
    }else{
      this.goodsItemService.getGoodsStockByItemID(this.goodsRequestObj).subscribe((data)=>{
        this.goodsItemStock = data;
        console.log ('получен сток товара с сервера ', this.goodsItemStock);
      });
    }
    // forkJoin([this.])

    if(this.subcategoryUrl ==='summer'){
      forkJoin([this.goodsItemService.getGoodsItemByID({
        category:'papercraft',
        subcategory: 'paper',
        id: this.goodsRequestObj.id}),
        this.goodsItemService.getGoodsItemByID({
          category:'art',
          subcategory: 'coloring',
          id: this.goodsRequestObj.id})]).subscribe((data)=>{
          console.log('получили данные по товару от forkjoin ', data);
          const goodsArr = data;
          const filteredArr = goodsArr.filter((item)=>item!==null);
          console.log('объединение масива ', filteredArr);
          this.goodsItemObj = filteredArr[0];
          console.log(this.goodsItemObj);

          this.setBreadcrumbNav();
          this.setGoodsItemRating();
          this.setGoodsItemImages();

      });
    }else {
      this.goodsItemService.getGoodsItemByID(this.goodsRequestObj).subscribe((data) => {
      this.goodsItemObj = data;
      console.log('получили объект товара с сервера ', this.goodsItemObj);
      this.setBreadcrumbNav();
      this.setGoodsItemRating();
      this.setGoodsItemImages();

     });
    }

     if(this.goodsItemStock && this.goodsItemStock.stock==1 && this.goodsItemStock.stock !== this.goodsItemStock.reserve){
          this.plusButtonState = true;
          this.messageService.add({ severity: 'info', summary: 'Количество', detail: `Доступно: ${this.quantity.value}` });
        }

        if (this.goodsItemStock && this.goodsItemStock.stock == this.goodsItemStock.reserve){
          this.quantity.setValue(0);
          this.plusButtonState = true;
          this.minusButtonState = true;
        }
     }

  ngAfterViewInit() {
  }

  jumpTo(id:string|null){
    if(id){
      const doc = document?.getElementById(id);
      console.log('элемент по ссылке ', doc);
      doc?.scrollIntoView({behavior:"smooth"});
    }
  }
  goToRatingSection(){
    this.jumpTo(this.fragment);
  }

  evaluateGoodsItem(){
     switch (this.setRating){
       case 1: this.goodsItemObj!.rating1++;
       break;
       case 2: this.goodsItemObj!.rating2++;
       break;
       case 3: this.goodsItemObj!.rating3++;
       break;
       case 4: this.goodsItemObj!.rating4++;
       break;
       case 5: this.goodsItemObj!.rating5++;
       break;
       default: {
         console.log('товар не оценен');
         this.messageService.add({ severity: 'warn', summary: 'Товар не оценен!'});
       }
    }
    console.log('измененный рейтинг в объекте товара ', this.goodsItemObj);
    const ratingBody = {
      rating1: this.goodsItemObj?.rating1,
      rating2: this.goodsItemObj?.rating2,
      rating3: this.goodsItemObj?.rating3,
      rating4: this.goodsItemObj?.rating4,
      rating5: this.goodsItemObj?.rating5
    }
    if(this.setRating){
      this.goodsItemService.setGoodsItemRating(this.goodsRequestObj,ratingBody).subscribe((data)=>{
        const updatedGoodsItem = data;
        console.log('получили обновленный товар ', updatedGoodsItem);
        if(updatedGoodsItem){
          this.messageService.add({severity: 'success', summary: 'Спасибо за вашу оценку!'});
        }
      });
    }
  }

  decreaseQuantity(){
   let quantityValue = this.quantity.value;
    if(quantityValue){
      console.log('текущее значение количества при минус ', quantityValue);
      quantityValue--;
      if(quantityValue == 1){
        this.minusButtonState = true;
        this.quantity.setValue(quantityValue);
      }else{
        this.minusButtonState = false;
        this.quantity.setValue(quantityValue);
      }
      this.plusButtonState = false;
    }
 }

 increaseQuantity(){
    let quantityValue = this.quantity.value;
    if(quantityValue) {
      console.log('текущее значение количества при плюс ', quantityValue);
      quantityValue++;
      if (this.goodsItemStock && quantityValue == this.goodsItemStock.stock - this.goodsItemStock.reserve) {
        this.plusButtonState = true;
        this.quantity.setValue(quantityValue);
        this.messageService.add({ severity: 'info', summary: 'Количество', detail: `Доступно: ${this.quantity.value}` });
      } else {
        this.plusButtonState = false;
        this.quantity.setValue(quantityValue);
       }
      this.minusButtonState = false;
    }
  }

  goodsItemToCart(){

    //ветка, если корзина уже создана
    if (this.cartService.uniqueID || localStorage.getItem('UID')){
      this.cartUID = this.cartService.uniqueID;  //присваиваем доступный UID
      this.cartUID = localStorage.getItem('UID');
      console.log('Корзина создана с UID ', this.cartUID);
      const cartItemID = this.goodsItemId || localStorage.getItem('goodsItemId');
      console.log('Добавить в корзину товар с ид ', cartItemID);

      //кладем товар в корзину, если есть в наличии ( не равно 0)
      if (this.quantity.value){
        const cartItem = {
          itemID:  cartItemID,
          quantity: this.quantity.value,
          category: this.categoryUrl,
          subcategory: this.subcategoryUrl
        }
        console.log('Объект товара для корзины: ', cartItem);
        this.cartService.addGoodsItemToCart(this.cartUID,cartItem).subscribe((data)=>{
          const goodsItemInCart = data;
          console.log('Положили товар в корзину ', goodsItemInCart);
          if (goodsItemInCart){
            this.messageService.add({severity: 'success', summary: 'Товар добавлен в корзину'});
            const reserveBody = {
              itemId: cartItemID,
              stock: this.goodsItemStock!.stock,
              reserve: this.quantity.value
            }
            console.log('Сформировали объект для резерва ', reserveBody);
            console.log('Объект запроса на сервер ', this.goodsRequestObj);
            this.goodsItemService.setGoodsStockReserve(this.goodsRequestObj,reserveBody).subscribe((data)=>{
            this.goodsItemStock = data;
              console.log('Получили обновленный резер ', this.goodsItemStock);
            if (this.goodsItemStock.stock == this.goodsItemStock.reserve){
                this.quantity.setValue(0);
                this.plusButtonState = true;
                this.minusButtonState = true;
              }
            });
          }
        });

        //если нет товара в наличии
      }else{
        console.log('Мы зашли в эту ветку, товара нет в наличии');
        this.messageService.add({ severity: 'error', summary: 'Товара нет в наличии'});
      }

      //ветка, если корзина еще не создана
    } else{
      this.cartUID = this.cartService.generateClientUID();
      console.log('Создали новый UID для корзины ', this.cartUID);
      const cartItemID = this.goodsItemId || localStorage.getItem('goodsItemId');
      console.log('Добавить в корзину товар с ид ', cartItemID);
      const cartItem = {
        UID: this.cartUID,
        itemID:  cartItemID,
        quantity: this.quantity.value,
        category: this.categoryUrl,
        subcategory: this.subcategoryUrl
      }
      console.log('Объект товара для корзины: ', cartItem);
      this.cartService.createCartforUID(cartItem).subscribe((data)=>{
        const goodsItemInCart = data;
        console.log('Положили товар в корзину ', goodsItemInCart);
        if (goodsItemInCart) {
          this.messageService.add({severity: 'success', summary: 'Товар добавлен в корзину'});
          const reserveBody = {
            itemId: cartItemID,
            stock: this.goodsItemStock!.stock,
            reserve: this.quantity.value
          }
          console.log('Сформировали объект для резерва ', reserveBody);
          console.log('Объект запроса на сервер ', this.goodsRequestObj);
          this.goodsItemService.setGoodsStockReserve(this.goodsRequestObj,reserveBody).subscribe((data)=>{
            this.goodsItemStock = data;
            console.log('Получили обновленный резер ', this.goodsItemStock);
            if (this.goodsItemStock.stock == this.goodsItemStock.reserve){
              this.quantity.setValue(0);
              this.plusButtonState = true;
              this.minusButtonState = true;
            }
        });
        }else{
          console.log('Мы зашли в эту ветку, товара нет в наличии');
          this.messageService.add({ severity: 'error', summary: 'Товара нет в наличии'});
        }
      });
    }

  }

  setBreadcrumbNav(){
    if(this.goodsItemObj){
      if(this.subcategoryUrl==='summer'){
        this.items = [{
          label: 'Лето',
          routerLink: ['/handmade', this.categoryUrl, this.subcategoryUrl]
        },
         {label: this.goodsItemObj?.title}];
        this.home = {icon: 'pi pi-home', routerLink: '/'};
      }else{
        this.items = [{
          label: this.goodsItemObj?.category,
          routerLink: ['/handmade', this.categoryUrl, this.subcategoryUrl]
        },
          {
            label: this.goodsItemObj?.subcategory,
            routerLink: ['/handmade', this.categoryUrl, this.subcategoryUrl]
          },
          {label: this.goodsItemObj?.title}];
        this.home = {icon: 'pi pi-home', routerLink: '/'};
      }
   }
  }

  setGoodsItemRating(){
    if (this.goodsItemObj) {
      this.goodsList.push(this.goodsItemObj);
    }
    this.goodsList = this.goodsListPageService.countGoodsTotalRating(this.goodsList);
    this.goodsItemObj = this.goodsList[0];
    console.log('объект товара с рейтингом ', this.goodsItemObj);
  }

  setGoodsItemImages(){
    const tempimg = [{
      itemImageSrc: "http://localhost:3000/public/" + this.categoryUrl +'/'+ this.subcategoryUrl +'/'+ this.goodsItemObj?.img,
      thumbnailImageSrc: "http://localhost:3000/public/" + this.categoryUrl +'/' + this.subcategoryUrl +'/'+ this.goodsItemObj?.img,
    },
      {
        itemImageSrc: "http://localhost:3000/public/" + this.categoryUrl +'/'+ this.subcategoryUrl +'/' + this.goodsItemObj?.img_thumb,
        thumbnailImageSrc: "http://localhost:3000/public/" + this.categoryUrl + '/'+ this.subcategoryUrl +'/' + this.goodsItemObj?.img_thumb,
      }
    ];
    this.images = tempimg;
  }

 /* generateClientUID(): string{
    const uniqueID = crypto.randomUUID();
    console.log('сформировали уникальный id ', uniqueID);
    return uniqueID;

  }*/

}
