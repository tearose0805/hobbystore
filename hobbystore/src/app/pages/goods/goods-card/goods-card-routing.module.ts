import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GoodsCardComponent } from './goods-card.component';

const routes: Routes = [{ path: '', component: GoodsCardComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GoodsCardRoutingModule { }
