import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { GoodsCardRoutingModule } from './goods-card-routing.module';
import { GoodsCardComponent } from './goods-card.component';
import { GalleriaModule } from 'primeng/galleria';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { RatingModule } from 'primeng/rating';
import { AccordionModule } from 'primeng/accordion';
import {ToastModule} from 'primeng/toast';
import {MessageService} from "primeng/api";



@NgModule({
  declarations: [
    GoodsCardComponent
  ],
  imports: [
    CommonModule,
    BreadcrumbModule,
    GalleriaModule,
    FormsModule,
    ReactiveFormsModule,
    RatingModule,
    AccordionModule,
    ToastModule,
    GoodsCardRoutingModule
  ],
  providers: [MessageService]
})
export class GoodsCardModule { }
