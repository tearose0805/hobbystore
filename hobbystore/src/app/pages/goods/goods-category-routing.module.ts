import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {GoodsCategoryComponent} from "./goods-category.component";

const routes: Routes = [
  { path: '', component: GoodsCategoryComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GoodsCategoryRoutingModule { }
