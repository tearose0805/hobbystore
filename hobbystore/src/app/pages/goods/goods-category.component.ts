import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {MenuItem} from "primeng/api";
import {ActivatedRoute, ParamMap} from '@angular/router'
import {IMenuCategoryInfo} from "../../models/categories/main-menu-categories";
import {GoodsCategoryService} from "../../services/goods/goods-category.service";
import {Subscription} from "rxjs";
import {GoodsListPageService} from "../../services/goods/goods-list-page.service";

@Component({
  selector: 'app-goods-category',
  templateUrl: './goods-category.component.html',
  styleUrls: ['./goods-category.component.scss']
})
export class GoodsCategoryComponent implements OnInit, AfterViewInit, OnDestroy{

  items: MenuItem[] =[];
  home: MenuItem = {};

  categoryUrl: string|null = '';
  subcategoryUrl: string|null = '';
  subcategoryTitle: string = '';
  subcategoryDescription: string = '';

  goodsCategoriesCollection:IMenuCategoryInfo[] = [];
  menuCategoryObj: any;
  navOptionSub: Subscription | undefined;
  navState:boolean = false;


  constructor(private route: ActivatedRoute,
              private goodsCategoryService: GoodsCategoryService,
              private goodsListPageService: GoodsListPageService,
             ) { }

  ngOnInit() {

      this.route.paramMap.subscribe((params:ParamMap) => {
      this.categoryUrl = params.get('category');
      this.subcategoryUrl = params.get ('subcategory');
      console.log ('категория из goodscategory : ', this.categoryUrl);
      console.log ('подкатегория из goodscategory:', this.subcategoryUrl);
    });

      this.goodsCategoryService.getGoodsCategories().subscribe((data)=>{
      this.goodsCategoriesCollection = data;
      console.log("получили массив пунктов меню ", this.goodsCategoriesCollection);
      this.menuCategoryObj = this.goodsCategoriesCollection.find ((item) =>item.subcategoryUrl===this.subcategoryUrl);
      console.log('нашли объект пункта меню ', this.menuCategoryObj);
        if(this.menuCategoryObj.subcategoryUrl === "summer") {
          this.items = [{label: this.menuCategoryObj.subcategory}];
          this.home = { icon: 'pi pi-home', routerLink: '/' };
          this.subcategoryTitle = this.menuCategoryObj.subcategory;
          this.subcategoryDescription = this.menuCategoryObj.description;
        } else {
          this.items = [{label: this. menuCategoryObj.category }, {label: this.menuCategoryObj.subcategory }];
          this.home = { icon: 'pi pi-home', routerLink: '/' };
          this.subcategoryTitle = this.menuCategoryObj.subcategory;
          this.subcategoryDescription = this.menuCategoryObj.description;
        }
    });

 }

 ngAfterViewInit() {
   this.navOptionSub = this.goodsListPageService.navOption$.subscribe((state) => {
     this.navState = state;
     console.log('изменение состояния пункта меню из goodscategory ', this.navState);

     setTimeout(() => {
       if (this.navState) {
         console.log('категорию для запроса из goodscategory ', this.categoryUrl);
         console.log('подкатегорию для запроса из goodscategory ', this.subcategoryUrl);
         this.menuCategoryObj = this.goodsCategoriesCollection.find ((item) =>item.subcategoryUrl===this.subcategoryUrl);
         console.log('нашли категории после смены пункта меню ', this.menuCategoryObj);

         if(this.menuCategoryObj.subcategoryUrl === "summer"){
           this.items = [{label: this.menuCategoryObj.subcategory }];
           this.home = { icon: 'pi pi-home', routerLink: '/' };
           this.subcategoryTitle = this.menuCategoryObj.subcategory;
           this.subcategoryDescription = this.menuCategoryObj.description;
         } else{
           this.items = [{label: this. menuCategoryObj.category }, {label: this.menuCategoryObj.subcategory }];
           this.home = { icon: 'pi pi-home', routerLink: '/' };
           this.subcategoryTitle = this.menuCategoryObj.subcategory;
           this.subcategoryDescription = this.menuCategoryObj.description;
         }
     }

   },100);
 });

}

ngOnDestroy() {
  this.navOptionSub?.unsubscribe();
}
}
