import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {ISortMenu} from "../../../models/goods/sort";
import {GoodsListService} from "../../../services/goods/goods-list.service";
import {IGoodsItemCard} from "../../../models/goods/goods-item-card";
import {GoodsListPageService} from "../../../services/goods/goods-list-page.service";
import {ActivatedRoute, ParamMap} from '@angular/router'
import {IFilterOption} from "../../../models/aside-filter/aside-filter-option";
import {debounceTime, forkJoin, Subscription} from "rxjs";
import {SearchCategoriesService} from "../../../services/search-categories/search-categories.service";




@Component({
  selector: 'app-goods-list',
  templateUrl: './goods-list.component.html',
  styleUrls: ['./goods-list.component.scss']
})
export class GoodsListComponent implements OnInit, OnDestroy, AfterViewInit{

  formGroup: FormGroup = new FormGroup({
  selectedItem: new FormControl<ISortMenu>({} )
  });
  sortMenuItems:ISortMenu[]=[];

  goodsList: IGoodsItemCard[] = [];
  testList: IGoodsItemCard[] = [];
  sortedList:IGoodsItemCard[] = [];
  defaultList:IGoodsItemCard[] = [];

  filterOptionsArr: IFilterOption[] = [];
  filterOptionsSub: Subscription | undefined;
  filteredGoodsList:any = [];

  navOptionSub: Subscription | undefined;
  navState:boolean = false;

  categoryUrl: string|null = '';
  subcategoryUrl: string|null = '';

  constructor(private goodsListService:GoodsListService,
              private goodsListPageService: GoodsListPageService,
              private searchCategoriesService: SearchCategoriesService,
              private route: ActivatedRoute) {
  }

  ngOnInit()  {
    this.sortMenuItems = [
      {name:'По умолчанию', code: 'Default' },
      {name: 'Сначала недорогие', code: 'LP'},
      {name: 'Сначала дорогие', code: 'HP'},
      {name: 'Популярные', code: 'Rate'}
    ];

     /* this.goodsListService.getPapercratGoodsList().subscribe((data:IGoodsCard[])=>{
        this.goodsList = data;
        console.log('получили список товаров ', this.goodsList);
        });*/

      this.route.paramMap.subscribe((params:ParamMap) => {
      this.categoryUrl = params.get('category');
      this.subcategoryUrl = params.get ('subcategory');
      console.log ('категория из goodslist: ', this.categoryUrl);
      console.log ('подкатегория из goodslist:', this.subcategoryUrl);
     // this.updateGoodsList();
    });
      if (this.subcategoryUrl === "summer"){
       forkJoin([this.searchCategoriesService.searchGoods(
         {category:'papercraft',
         subcategory: 'paper',
         searchOption: 'картонный конструктор'}),

       this.searchCategoriesService.searchGoods({
         category: 'art',
         subcategory: 'coloring',
         searchOption: 'витраж'}),

         this.searchCategoriesService.searchGoods({
           category: 'art',
           subcategory: 'coloring',
           searchOption: 'роспись по дереву'})]).subscribe((data:any)=>{
           console.log('получили объект по summer ', data);
           const goodsArr = data;
           const flatArr = goodsArr.flat();
           console.log('объединение масива ', flatArr);
          this.testList = [...flatArr];
          this.goodsList = this.goodsListPageService.countGoodsTotalRating(this.testList);
          this.defaultList = [...this.goodsList];
        });
      } else{
        this.goodsListService.testRequest(this.categoryUrl,this.subcategoryUrl).subscribe((data)=>{
          console.log('тестовый объект с сервера ', data);
          const goodsArr = data;
          this.testList = [...goodsArr];
          this.goodsList = this.goodsListPageService.countGoodsTotalRating(this.testList);
          this.defaultList = [...this.goodsList];
        });
      }

      if(this.filterOptionsSub){
      this.filterOptionsSub.unsubscribe();
    }
   }

   ngAfterViewInit() {
     this.filterOptionsSub = this.goodsListPageService.filterOption$.pipe(debounceTime(1300)).subscribe((data: IFilterOption[]) => {
       console.log('получили опции из фильтра ', data);
       this.filterOptionsArr = data;
       console.log('записали опции фильтра в массив ', this.filterOptionsArr);
       this.filterGoodsList();
     });

  this.navOptionSub = this.goodsListPageService.navOption$.subscribe((state) => {
       this.navState = state;
       console.log('вывели изменение состояния пункта меню ', this.navState);
       this.goodsList = [];

       setTimeout(() =>{
         if (this.navState) {
           console.log('вывести категорию для запроса на сервер ', this.categoryUrl);
           console.log('вывести подкатегорию товаров для запроса на сервер ', this.subcategoryUrl);
           if (this.subcategoryUrl === "summer"){
             forkJoin([this.searchCategoriesService.searchGoods(
               {category:'papercraft',
                 subcategory: 'paper',
                 searchOption: 'картонный конструктор'}),

               this.searchCategoriesService.searchGoods({
                 category: 'art',
                 subcategory: 'coloring',
                 searchOption: 'витраж'}),

               this.searchCategoriesService.searchGoods({
                 category: 'art',
                 subcategory: 'coloring',
                 searchOption: 'роспись по дереву'})]).subscribe((data:any)=>{
               console.log('получили объект по summer ', data);
               const goodsArr = data;
               const flatArr = goodsArr.flat();
               console.log('объединение масива ', flatArr);
               this.testList = [...flatArr];
               this.goodsList = this.goodsListPageService.countGoodsTotalRating(this.testList);
               this.defaultList = [...this.goodsList];
             });
           }else{
             this.goodsListService.testRequest(this.categoryUrl, this.subcategoryUrl).subscribe((data) => {
               const goodsArr = data;
               console.log('получили список товаров с сервера после смены пункта меню ', goodsArr);
               this.testList = [...goodsArr];
               this.goodsList = this.goodsListPageService.countGoodsTotalRating(this.testList);
               this.defaultList = [...this.goodsList];
             });
           }
         }
       },100);
     });
  }

  sortGoodsList(){
     const sortOption = this.formGroup.value;
       if(sortOption.selectedItem){
     console.log('вывели выбранную сортировку ', sortOption.selectedItem);
     const key = sortOption.selectedItem.code;
     console.log('вывести ключ сортировки ', key);
      if(key === "Default"){
        this.goodsList = this.defaultList;
       } else {
      this.sortedList = this.goodsListPageService.sortGoodsByLowPrice(this.goodsList,key);
      console.log('вывести отсортированный список ', this.sortedList);
      this.goodsList = this.sortedList;
          }
      }
   }

   filterGoodsList() {
     const totalFilteredArr: any = [];
     if (this.filterOptionsArr.length) {
       console.log('вывести длину массива для фильтрации ', this.filterOptionsArr.length);

       const typeOption = this.filterOptionsArr.find((option) => option.key === "type");
       const colorOption = this.filterOptionsArr.find((option) => option.key === "color");

       if (typeOption && !colorOption) {
         this.filterOptionsArr.forEach((option) => {
           const arr = [...this.defaultList];
           console.log('вывести опцию меню ', option);

           const resultFilteredArr = this.filterGoodsListByType(arr, option);
           totalFilteredArr.push(resultFilteredArr);
           console.log('вывели общий отфильтрованный массив массивов ', totalFilteredArr);
         });
         this.filteredGoodsList = totalFilteredArr.flat();
         console.log('вывели плоский массив ', this.filteredGoodsList);

       }
       else if (colorOption && !typeOption) {
         this.filterOptionsArr.forEach((option) => {
           const arr = [...this.defaultList];
           console.log('вывести опцию меню ', option);

           const resultFilteredArr = this.filterGoodsListByColor(arr, option);
           totalFilteredArr.push(resultFilteredArr);
           console.log('вывели общий отфильтрованный массив массивов ', totalFilteredArr);
         });
         this.filteredGoodsList = totalFilteredArr.flat();
         console.log('вывели плоский массив ', this.filteredGoodsList);

       }
       else if (colorOption && typeOption) {
         const totalArrByTypeAndColor: any = [];
         const filterTypeOptions = this.filterOptionsArr.filter((option) => option.key === "type");
         console.log('получили массив опций по типу ', filterTypeOptions);

         const filterColorOptions = this.filterOptionsArr.filter((option) => option.key === "color");
         console.log('получили массив опций по цвету ', filterColorOptions);

         filterTypeOptions.forEach((option) => {
           const arr = [...this.defaultList];
           console.log('вывести опцию меню ', option);

           const resultFilteredArr = this.filterGoodsListByType(arr, option);
           totalFilteredArr.push(resultFilteredArr);
           console.log('вывели общий отфильтрованный массив массивов ', totalFilteredArr);
         });
         this.filteredGoodsList = totalFilteredArr.flat();
         console.log('вывели плоский массив по типу ', this.filteredGoodsList);

         filterColorOptions.forEach((option)=>{
           const arr = [...this.filteredGoodsList];
           console.log('вывести опцию меню ', option);

           const resultFilteredArr = this.filterGoodsListByColor(arr, option);
           totalArrByTypeAndColor.push(resultFilteredArr);
           console.log('вывели общий отфильтрованный массив массивов ', totalArrByTypeAndColor);
           });
         this.filteredGoodsList = totalArrByTypeAndColor.flat();
         console.log('вывели плоский массив по типу  и цвету', this.filteredGoodsList);
       }

       this.goodsList = this.filteredGoodsList;
     }else{
       this.goodsList = this.defaultList;
     }
   }

  /*    this.filteredGoodsList = totalFilteredArr.flat();
      console.log('вывели плоский массив ',this.filteredGoodsList);

    }else {

    }
  }*/

  filterGoodsListByType(arr:IGoodsItemCard[],option:IFilterOption){
    return arr.filter((item)=>option.name.toLowerCase() === item.type.toLowerCase());
  }

   filterGoodsListByColor(arr:IGoodsItemCard[],option:IFilterOption){
   // return arr.filter((item)=>option.name.toLowerCase() === item.color?.toLowerCase());
     return arr.filter((item)=>item.color?.toLowerCase().includes(option.name.toLowerCase()));
  }

   ngOnDestroy() {
    this.filterOptionsSub?.unsubscribe();
    this.navOptionSub?.unsubscribe();
   }
}
