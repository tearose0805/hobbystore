import {AfterViewInit, Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {IFilterOption} from "../../../models/aside-filter/aside-filter-option";
import {GoodsListPageService} from "../../../services/goods/goods-list-page.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-aside-filter',
  templateUrl: './aside-filter.component.html',
  styleUrls: ['./aside-filter.component.scss']
})
export class AsideFilterComponent implements OnInit, AfterViewInit{

  navOptionSub: Subscription | undefined;
  navState:boolean = false;

  selectedCategories: any[] = [];

  categories_type: any[] = [];
  categories_color: any[] = [];

  paper: any[] =[
    { name: 'Картонный конструктор', key: 'type' },
    { name: 'Цветная бумага', key: 'type' },
    { name: 'Открытки', key: 'type' },
    { name: 'Стикеры', key: 'type' }
  ];

  flower: any[]=[
    { name: 'Бумажные цветы', key: 'type' },
    ];

  artsupply: any[]=[
    { name: 'Цветные карандаши', key: 'type' },
    { name: 'Фломастеры', key: 'type' },
    { name: 'Мелки', key: 'type' },
    { name: 'Краски', key: 'type' }
  ];

  coloring: any[]=[
    { name: 'Раскраска', key: 'type' },
    { name: 'Керамика', key: 'type' },
    { name: 'Витраж', key: 'type' },
    { name: 'Роспись по дереву', key: 'type' }
  ]
  partysupply: any[]=[
    { name: 'Воздушные шары', key: 'type' },
    { name: 'Упаковочная бумага', key: 'type' },
    { name: 'Лента декоративная', key: 'type' },
    { name: 'Гирлянда', key: 'type' }
  ]

  summer: any[]=[
    { name: 'Картонный конструктор', key: 'type' },
    { name: 'Витраж', key: 'type' },
    { name: 'Роспись по дереву', key: 'type' },
     ];

  color: any[] =[
    { name: 'Белый', key: 'color' },
    { name: 'Синий', key: 'color' },
    { name: 'Зеленый', key: 'color' },
    { name: 'Кремовый', key: 'color' },
    { name: 'Розовый', key: 'color' },
    { name: 'Многоцветный', key: 'color' },
  ];



  categoryUrl: string | null = '';
  subcategoryUrl: string | null = '';

  constructor(private router: Router,
              private route: ActivatedRoute,
              private goodsListPageService: GoodsListPageService){}

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.categoryUrl = params.get('category');
      this.subcategoryUrl = params.get('subcategory');
      console.log('категория : ', this.categoryUrl);
      console.log('подкатегория :', this.subcategoryUrl);
    });

    this.navOptionSub = this.goodsListPageService.navOption$.subscribe((state) => {
      this.navState = state;
      console.log('вывели изменение состояния пункта меню  из asidefilter', this.navState);
    });
    this.setFilterOptions();
    }

 ngAfterViewInit() {
   this.navOptionSub = this.goodsListPageService.navOption$.subscribe((state) => {
     this.navState = state;
     console.log('вывели изменение состояния пункта меню  из asidefilter', this.navState);

     if(this.navState){
       setTimeout(()=>{
         console.log('вывести category asidefilter ', this.categoryUrl);
         console.log('вывести subcategory asidefilter ', this.subcategoryUrl);
         this.setFilterOptions();
       },200);
     }
   });
  }

 setFilterOptions(){
   switch(this.subcategoryUrl){
     case 'paper':{
       this.categories_type = this.paper;
       this.categories_color = this.color;
       break;
     }
     case 'paper-flower':{
       this.categories_type = this.flower;
       this.categories_color = this.color;
       break;
     }
     case 'artsupply':{
       this.categories_type = this.artsupply;
       this.categories_color = this.color;
       break;
     }
     case 'coloring':{
       this.categories_type = this.coloring;
       this.categories_color = this.color;
       break;
     }
     case 'partysupply':{
       this.categories_type = this.partysupply;
       this.categories_color = this.color;
       break;
     }
     case 'summer':{
       this.categories_type = this.summer;
       this.categories_color = this.color;
       break;
     }
     default : {
       break;
     }
   }
 }

  filterGoodsList(option:IFilterOption[]){
    // console.log('вывести event фильтра ', ev);
     console.log('вывести  массив пунктов фильтра ', option);
    this.goodsListPageService.getAsideFilterOption(option);
  }



}
