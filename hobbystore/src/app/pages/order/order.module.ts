import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderRoutingModule } from './order-routing.module';
import { OrderComponent } from './order.component';
import {ReactiveFormsModule} from "@angular/forms";
import { TableModule } from 'primeng/table';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { InputMaskModule } from 'primeng/inputmask';
import { InputTextModule } from 'primeng/inputtext';
import{FormsModule} from "@angular/forms";
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';


@NgModule({
  declarations: [
    OrderComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    TableModule,
    BreadcrumbModule,
    InputMaskModule,
    InputTextModule,
    FormsModule,
    DialogModule,
    ButtonModule,
    OrderRoutingModule
  ]
})
export class OrderModule { }
