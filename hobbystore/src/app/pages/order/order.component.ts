import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import { FormGroup, FormControl } from '@angular/forms';
import {OrderPageService} from "../../services/order/order-page.service";
import { Validators } from '@angular/forms';
import {ICustomer} from "../../models/customer/customer";
import {MenuItem} from "primeng/api";
import {OrderService} from "../../services/order/order.service";

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit{

  constructor(private router: Router,
              private orderPageService: OrderPageService,
              private orderService: OrderService) {
  }
 orderForm = new FormGroup({
    firstName: new FormControl('',{validators: Validators.required}),
    secondName: new FormControl (''),
    lastName : new FormControl('',{validators: Validators.required}),
    mobile: new FormControl<string | null>(null),
    index: new FormControl<string | null>(null),
    region: new FormControl ('',{validators: Validators.required}),
    city: new FormControl ('',{validators: Validators.required}),
    address: new FormControl ('',{validators: Validators.required})
});

  orderTotal: any=[];
  orderObj: any;
  orderNumber: number =0;
  maxNumber = 100000;
  minNumber =10000;

  items: MenuItem[] =[];
  home: MenuItem ={};

  required: boolean= true;
  visible: boolean = false;

  ngOnInit() {

    this.items = [{label: "Корзина", routerLink: ['/handmade/cart']},{label: "Заказ"}];
    this.home = {icon: 'pi pi-home', routerLink: '/'};
    this.getOrder();
    }

  onSubmit(){
   // console.log(this.orderForm.value);
    console.log(this.orderForm.status);
  }
  getOrder(){
    this.orderObj = this.orderPageService.getOderInfo();
     console.log('получили объект заказа из сервиса /local storage ', this.orderObj);
     this.orderTotal = this.orderObj.orderInfo;
     console.log('сохранили информацию о заказе ', this.orderTotal);
     console.log(this.orderForm.value);
     console.log(this.orderForm.status);
    }

    initOrder(){
     const customerData = this.orderForm.getRawValue();
      if(customerData){
        const customerObj: ICustomer ={
          firstName: customerData.firstName,
          secondName: customerData.secondName,
          lastName : customerData.lastName,
          mobile: customerData.mobile,
          index: customerData.index,
          region: customerData.region,
          city: customerData.city,
          address: customerData.address
        }
        this.orderNumber = Math.floor(Math.random() * (this.maxNumber - this.minNumber) + this.minNumber);
        console.log ('вывели номер заказа ', this.orderNumber);
        console.log('вывели объект покупателя ', customerData);

        const completeOrder ={...this.orderObj,...customerObj, orderNumber: this.orderNumber};
        console.log('вывели полный объект заказа ', completeOrder);

        this.orderService.createOrder(completeOrder).subscribe((data)=>{});
      }
      this.showDialog();
      this.orderPageService.deleleOrder();
   }

   isValid(control: string){
    return this.orderForm.get(control)?.invalid && this.orderForm.get(control)?.touched;
       }

  showDialog() {
    this.visible = true;
  }

  closeOrderForm(){
    this.router.navigate(['/']);
  }
}
