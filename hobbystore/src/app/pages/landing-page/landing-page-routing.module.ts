import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LandingPageComponent} from "./landing-page.component";

const routes: Routes = [
  { path: '', component: LandingPageComponent,
    children: [
      {
        path: '',
        loadChildren: ()  => import('../home/home.module').then(m => m.HomeModule)
      },
 /*     {
        path: 'cart/order',
        loadChildren: () => import('../order/order.module').then(m => m.OrderModule)
      },*/
      {
        path: 'cart',
        loadChildren: ()  => import('../cart/cart.module').then(m => m.CartModule),
        /*    children:[
              {
                path: 'order',
                loadChildren: () => import('../order/order.module').then(m => m.OrderModule)
              }
      ]*/
      },
      {
        path: ':category/:subcategory',
        loadChildren: ()  => import('../goods/goods-category.module').then(m => m.GoodsCategoryModule)
      },
      {
        path: ':category/:subcategory/:id',
        loadChildren: ()  => import('../goods/goods-card/goods-card.module').then(m => m.GoodsCardModule)
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LandingPageRoutingModule { }
