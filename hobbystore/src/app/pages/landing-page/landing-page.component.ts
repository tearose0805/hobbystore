import {Component, OnInit} from '@angular/core';
import {MenuItem} from "primeng/api";
import {IMenuCategory} from "../../models/categories/main-menu-categories";
import {HomePageService} from "../../services/home-page/home-page.service";
import {GoodsListPageService} from "../../services/goods/goods-list-page.service";

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {
  items: MenuItem[] = [];

  constructor(private homePageService: HomePageService,
              private goodsListPageService:GoodsListPageService) {
  }


  ngOnInit() {
    this.items = [
      {
        label: 'Лето',
        icon: 'pi pi-sun',
        id:'discount',
        command:()=> {
          this.getNavOptionChangeState()
        },
        routerLink:['/handmade','feature','summer']
      },
      {
       label: 'Особые случаи',
       id:'occasion',
             items: [
               {
                 label: 'Товары для праздника',
                 command:()=> {
                   this.getNavOptionChangeState()
                 },
                 routerLink:['/handmade','occasion','partysupply']
               },
               {
                 label: 'Праздничные украшения',
                 routerLink:['#'],
                 visible: false
               },
               {
                 label: 'Подарки',
                 routerLink:['#'],
                 visible: false
               },
               {
                 label: 'Подарочная упаковка',
                 routerLink:['#'],
                 visible: false
               },
               {
                 label: 'Идеи для творчества',
                 routerLink:['#'],
                 visible: false
               },
             ]
          },
      {
        label: 'Творчество из бумаги',
            items: [
               {
                 label: 'Бумажная продукция',
                 command:()=> {
                 this.getNavOptionChangeState()
                 },
                 routerLink:['/handmade','papercraft','paper'],

                 },
              {
                 label: 'Инструменты и принадлежности',
                 routerLink:['#'],
                 visible: false
               },
               {
                 label: 'Аппликация',
                 routerLink:['#'],
                 visible: false
               },
               {
                 label: 'Поздравительные открытки',
                 routerLink:['#'],
                 visible: false
               },
               {
                 label: 'Бумажные цветы',
                 command:()=> {
                 this.getNavOptionChangeState()
                 },
                 routerLink:['/handmade','papercraft','paper-flower']
               },
               {
                 label: 'Оригами',
                 routerLink:['#'],
                 visible: false
               },
               {
                 label: 'Идеи для творчества',
                 routerLink:['#'],
                 visible: false
               },
               ]
           },

      {
        label: 'Рисование',
             items: [
                {
                  label: 'Кисти и краски',
                  routerLink:['#'],
                  visible: false
                },
                {
                  label: 'Рисование карандашами',
                  routerLink:['#'],
                  visible: false
                },
                {
                  label: 'Материалы для рисования',
                  command:()=> {
                    this.getNavOptionChangeState()
                  },
                  routerLink:['/handmade','art','artsupply']
                },
                {
                  label: 'Раскрась сам',
                  command:()=> {
                    this.getNavOptionChangeState()
                  },
                  routerLink:['/handmade','art','coloring']
                },
                {
                  label: 'Раскраски',
                  routerLink:['#'],
                  visible: false
                },
               {
                  label: 'Идеи для творчества',
                  routerLink:['#'],
                  visible: false
                },
        ]
      },

      {
        label: 'Декорирование',
           items: [
                {
                  label: 'Домашний декор',
                  routerLink:['#']
                },
                {
                  label: 'Декупаж',
                  routerLink:['#']
                },
                {
                  label: 'Освещение',
                  routerLink:['#']
                },
                {
                  label: 'Декоративные рамки',
                  routerLink:['#']
                },
                {
                  label: 'Идеи для творчества',
                  routerLink:['#']
                },
        ],
        visible: false
      },

      {
        label: 'Лепка',
        items: [
          {
            label: 'Полимерная глина',
            routerLink:['#']
          },
          {
            label: 'Пластилин',
            routerLink:['#']
          },
          {
            label: 'Принадлежности для лепки',
            routerLink:['#']
          },
          {
            label: 'Идеи для творчества',
            routerLink:['#']
          },
        ],
        visible: false
      },
      {
        label: 'Вышивка',
        items: [
          {
            label: 'Наборы для вышивки',
            routerLink:['#']
          },
          {
            label: 'Узоры для вышивки',
            routerLink:['#']
          },
          {
            label: 'Нитки и иглы для вышивки',
            routerLink:['#']
          },
          {
            label: 'Аксессуары',
            routerLink:['#']
          },
          {
            label: 'Идеи для творчества',
            routerLink:['#']
          },
        ],
        visible: false
      },
      {
        label: 'Вязание',
        items: [
          {
            label: 'Вязание спицами',
            routerLink:['#']
          },
          {
            label: 'Вязание крючком',
            routerLink:['#']
          },
          {
            label: 'Пряжа',
            routerLink:['#']
          },
          {
            label: 'Макраме',
            routerLink:['#']
          },
          {
            label: 'Принадлежности для вязания',
            routerLink:['#']
          },
          {
            label: 'Идеи для творчества',
            routerLink:['#']
          },
        ],
        visible: false
      },
      {
        label: 'Биссероплетение',
        items: [
          {
            label: 'Бисер и бусины',
            routerLink:['#']
          },
          {
            label: 'Принадлежности и инструменты',
            routerLink:['#']
          },
          {
            label: 'Фурнитура',
            routerLink:['#']
          },
          {
            label: 'Идеи для творчества',
            routerLink:['#']
          },
        ],
        visible: false
      },
      {
        label: 'Идеи для творчества',
        icon: 'pi pi-sun',
        id:'idea',
        items: [
          {
            label: 'Осень',
            routerLink:['#']
          },
          {
            label: 'Лето',
            routerLink:['#']
          },
          {
            label: 'Новый год',
            routerLink:['#']
          },
          {
            label: 'Идеи подарков',
            routerLink:['#']
          },
          {
            label: 'Школьные проекты',
            routerLink:['#']
          },
          {
            label: 'Идеи для рисования',
            routerLink:['#']
          },
        ],
        visible: false
    }
    ]
  }

  getNavOptionChangeState():void{
    this.goodsListPageService.getNavOptionChangeState(true);
  }

  }
