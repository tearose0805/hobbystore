import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {LandingPageRoutingModule } from './landing-page-routing.module';
import {LandingPageComponent} from "./landing-page.component";
import {HeaderComponent} from "../header/header.component";
import {FooterComponent} from "../footer/footer.component";
import {SearchModalComponent} from "../search-modal/search-modal.component";
import {FormsModule} from "@angular/forms";
import { ReactiveFormsModule } from '@angular/forms';
import { SidebarModule } from 'primeng/sidebar';
import { MenubarModule } from 'primeng/menubar';
import { ButtonModule } from 'primeng/button';



@NgModule({
  declarations: [
    LandingPageComponent,
    HeaderComponent,
    FooterComponent,
    SearchModalComponent
  ],
  imports: [
    CommonModule,
    LandingPageRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SidebarModule,
    MenubarModule,
    ButtonModule
   ],
})
export class LandingPageModule { }
