import {Component, OnDestroy, OnInit} from '@angular/core';
import {GoodsListPageService} from "../../services/goods/goods-list-page.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],

})
export class HeaderComponent implements OnInit, OnDestroy{
  svgStyleOnHover: string ="";
  iconId: string = '';
  sidebarVisible: boolean = false;

  constructor(private goodsListPageService: GoodsListPageService) {
  }

  ngOnInit() {
    this.goodsListPageService.modalClosed$.subscribe((data)=>{
      console.log('вывели состояние модал из header ', data);
      this.sidebarVisible = data;
    });
  }

  changeSVGStyle(ev:MouseEvent,el:HTMLElement):void{
    //console.log("События на иконках в header ", ev); //для контроля
    //console.log('иконка <a> el ', el);               //для контроля
    this.iconId = el.id;
    this.svgStyleOnHover= ev.type ==="mouseover" ? '#6CBEC2': '#30A8B4'
  }

  sidebarState():void{
    this.sidebarVisible = true;
  }


  ngOnDestroy():void{

  }



}
