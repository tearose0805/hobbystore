import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl} from "@angular/forms";
import {debounceTime, forkJoin, Observable, Subscription} from "rxjs";
import {SearchCategoriesService} from "../../services/search-categories/search-categories.service";
import {ISearchCategories} from "../../models/categories/search-categories"
import {IGoodsCategories} from "../../models/categories/goods-categories";
import {SearchModalService} from "../../services/search-modal/search-modal.service";
import {IGoodsItemCard} from "../../models/goods/goods-item-card";
import {GoodsCategoryService} from "../../services/goods/goods-category.service";
import {GoodsItemService} from "../../services/goods/goods-item.service";
import {GoodsListPageService} from "../../services/goods/goods-list-page.service";

@Component({
  selector: 'app-search-modal',
  templateUrl: './search-modal.component.html',
  styleUrls: ['./search-modal.component.scss']
})
export class SearchModalComponent implements OnInit, AfterViewInit,OnDestroy{
 userSearchInput = new FormControl('');

 searchCategories: Subscription | undefined;
// searchCategoriesCollection: ISearchCategories[] | undefined;
 //sixPopularSearches:ISearchCategories[] | undefined;

 goodsCategories: Subscription|undefined;
 goodsCategoriesCollection:any;
 //fivePopularCategories:IGoodsCategories[]|undefined;

 userInputSubscription: Subscription | undefined;
 searchGoodsSubscription: Subscription | undefined;

 goodsItemRequestArr: Observable<IGoodsItemCard>[] = [];

 goodsItemSearchResult: IGoodsItemCard[] = [];
 goodsItemSearchDisplay: IGoodsItemCard[] = [];
 goodsItemSearchCol:any = [];


 goodsIndex: number[]=[];


 goodsCategoryArr:any = [];

 constructor(private searchCategoriesService: SearchCategoriesService,
             private searchModalService: SearchModalService,
             private goodsCategoryService: GoodsCategoryService,
             private goodsListPageService: GoodsListPageService) {
 }

 ngOnInit() {
   this.goodsCategoryService.getGoodsCategories().subscribe((data)=> {
     this.goodsCategoriesCollection = data;
     console.log('вывели массив ', this.goodsCategoriesCollection);

   });
 //  this.getSearchCategories(); //этот метод не нужен
 //  this.getGoodsCategories(); //этот метод не нужен

 }


  ngAfterViewInit() {
    this.userInputSubscription = this.userSearchInput.valueChanges. pipe(debounceTime(1000)).subscribe((data)=>{
      console.log('вывести пользовательский ввод ', data);
      this.initSearchGoods(data);
  });
  }

  clearButtonVisible():boolean{
   return !this.userSearchInput.value;
 }

 initSearchGoods(data:string|null){
      if(data && data.length>=3){
     console.log('приняли ввод для поиска ', data);

      if(this.userInputSubscription && !this.searchGoodsSubscription?.closed){
        this.searchGoodsSubscription?.unsubscribe();
      }

     if(this.goodsCategoriesCollection){
       const catArr = this.goodsCategoriesCollection.filter((item:any)=>item.subcategoryUrl!== "summer");
       this.goodsItemRequestArr = catArr.map((catObj:any)=>{
             const requestObj = {
             category: catObj.categoryUrl,
             subcategory: catObj.subcategoryUrl,
             searchOption: data
           }
           console.log('Объект для запроса на сервер ', requestObj);
           return this.searchCategoriesService.searchGoods(requestObj);
       });
          console.log('массив observable ', this.goodsItemRequestArr);
     }

    this.searchGoodsSubscription = forkJoin(this.goodsItemRequestArr).subscribe((data:any)=> {
      console.log('получен поиск товаров ', data);

      //здесь получен массив массивов
     const rawGoodsArr = data;
     const filteredArr = rawGoodsArr.filter((item: IGoodsItemCard[]) => item.length !== 0);
     console.log('отфильтровали пустые массивы ', filteredArr);

        if(filteredArr.length){

        this.goodsItemSearchResult = filteredArr.flat();
        console.log('получили один целый массив ', this.goodsItemSearchResult);

        //отделили первые два элемента общего массива
        this.goodsItemSearchDisplay = this.goodsItemSearchResult.slice(0, 2);
        // @ts-ignore

        //добавили в массив последний элемент общего массива
        //должны получить массив не больше 3х элементов

        const arrLastEl = this.goodsItemSearchResult.pop();
        const goodsItemObj = this.goodsItemSearchDisplay.find((item)=>item._id === arrLastEl?._id);
        if(!goodsItemObj){
          // @ts-ignore
        this.goodsItemSearchDisplay.push(arrLastEl);
        console.log('вывели окончательный массив из max 3 эл ', this.goodsItemSearchDisplay);
        }
        this.goodsItemSearchCol = this.goodsItemSearchDisplay.map((item: any) => {
          const catObj = this.goodsCategoriesCollection.find((cat: any) => item.subcategory === cat.subcategory);
          console.log('вывести найденный объект категории ', catObj);

          //добавляем новые поля в объект товара
          item.categoryUrl = catObj.categoryUrl;
          item.subcategoryUrl = catObj.subcategoryUrl;

          //ищем в массиве отобранных категорий повторяющиеся объекты категорий
          const categoryObj = this.goodsCategoryArr.find((categoryItem: any) => categoryItem.subcategory === item.subcategory);

          //добавляем в массив объект категории
          if (!categoryObj) {
            this.goodsCategoryArr.push({
              category: item.category,
              categoryUrl: catObj.categoryUrl,
              subcategory: item.subcategory,
              subcategoryUrl: catObj.subcategoryUrl
            });
          }
          return item;
        });
        console.log('вывели сформированный массив товаров ', this.goodsItemSearchCol);
        console.log('вывели сформированный массив категорий ', this.goodsCategoryArr);
      }
    });
    }else{
     console.log('ввод меньше 3х символов');
   }
 }

 /*getSearchCategories():void{
   this.searchCategories = this.searchCategoriesService.getSearchCategories().
   subscribe((data:ISearchCategories[]) =>{
   console.log(data);
   this.searchCategoriesCollection=data;
   console.log('массив категорий поиска', this.searchCategoriesCollection);

   if(this.searchCategoriesCollection){
   const popularSearches = this.searchModalService.sortCategories(this.searchCategoriesCollection);
   console.log('отсортированный массив поисков: ',popularSearches);

   this.sixPopularSearches = this.searchModalService.getPopularCategories(popularSearches,6);
   console.log('массив первых 6 поисков: ', this.sixPopularSearches);
   }
  });
  }*/

/*  getGoodsCategories():void{
    this.goodsCategories = this.searchCategoriesService.getGoodsCategories().
    subscribe((data:IGoodsCategories[]) =>{
      console.log(data);
      this.goodsCategoriesCollection=data;
      console.log('массив категорий товаров', this.goodsCategoriesCollection);
      if(this.goodsCategoriesCollection){
        const popularCategories = this.searchModalService.sortCategories(this.goodsCategoriesCollection);
        console.log('отсортированный массив поисков: ',popularCategories);

        this.fivePopularCategories = this.searchModalService.getPopularCategories(popularCategories,5);
        console.log('массив первых 5 категорий: ', this.fivePopularCategories);
      }
   });
  }*/

 /* getIndex():number{
     const maxNumber = this.goodsItemSearchResult.length;
     let index = Math.floor(Math.random()*maxNumber);
     while (this.goodsIndex.includes(index)){
     index =  Math.floor(Math.random()*maxNumber);
     }
     this.goodsIndex.push(index);
     return index;
     }*/

     getModalVisibility(state:boolean){
      this.goodsItemSearchCol = [];
      this.goodsCategoryArr = [];
      this.userSearchInput.reset();
      this.goodsListPageService.getModalVisibility(state);
     }

     clearSearchModal(){
       this.goodsItemSearchCol = [];
       this.goodsCategoryArr = [];
       this.userSearchInput.reset();
     }

  ngOnDestroy():void{
   this.searchCategories?.unsubscribe();
   this.goodsCategories?.unsubscribe();
   this.userInputSubscription?.unsubscribe();
  }


}
