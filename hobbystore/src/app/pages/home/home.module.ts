import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import {HomeComponent} from "./home.component";
import { CarouselModule } from 'primeng/carousel';
import { BadgeModule } from 'primeng/badge';
import { ButtonModule } from 'primeng/button';
import { RatingModule } from 'primeng/rating';
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    CarouselModule,
    BadgeModule,
    ButtonModule,
    RatingModule,
    FormsModule,
   ]
})
export class HomeModule { }
