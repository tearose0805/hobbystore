import {Component, OnDestroy, OnInit} from '@angular/core';
import {IOffer} from "../../models/offers/home-page-offers";
import {HomePageService} from "../../services/home-page/home-page.service";
import {forkJoin, Observable, Subscription} from "rxjs";
import {IGoodsCard} from "../../models/goods/goods-card";
import {Router} from "@angular/router";
import {IGoodsItemCard} from "../../models/goods/goods-item-card";
import {SearchCategoriesService} from "../../services/search-categories/search-categories.service";
import {GoodsItemService} from "../../services/goods/goods-item.service";
import {GoodsListPageService} from "../../services/goods/goods-list-page.service";
import {GoodsCategoryService} from "../../services/goods/goods-category.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit,OnDestroy{
  offers: IOffer[] = [];
  bestChoices: IGoodsCard[] =[];

  bestChoiceGoods: IGoodsItemCard[] | undefined;
  bestChoiceGoodsColl: any=[];

  carouselOffersCollection: Subscription | undefined;
  carouselBestChoicesCollection: Subscription | undefined;
  value: number = 5;

  requestObjArr: Observable<IGoodsItemCard[]>[] = [];

  goodsCategoriesCollection: any = [];

  constructor(private homePageService: HomePageService,
              private searchCategoriesService: SearchCategoriesService,
              private goodsItemService: GoodsItemService,
              private goodsListPageService: GoodsListPageService,
              private goodsCategoryService: GoodsCategoryService,
              private router: Router) {
  }

  ngOnInit() {
    this.carouselOffersCollection = this.homePageService.getCarouselOffers().
    subscribe((data:IOffer[]) =>{
      console.log('для домашней карусели: ',data);
      setTimeout(()=>{
        this.offers = data;
      },100);
   });

    this.carouselBestChoicesCollection = this.homePageService.getCarouselBestChoices().
    subscribe((data:IGoodsCard[])=>{
      console.log ('для карусели отличный выбор ', data);
      this.bestChoices = data;
    });

    this.goodsCategoryService.getGoodsCategories().subscribe((data)=> {
      this.goodsCategoriesCollection = data;
      console.log('вывели массив ', this.goodsCategoriesCollection);
    });

    forkJoin([this.searchCategoriesService.searchGoods({
      category: 'papercraft',
      subcategory: 'paper',
      searchOption: 'конструктор'}),

      this.goodsItemService.getGoodsItemByID({
        category: 'papercraft',
        subcategory: 'paper',
        id: '64a270f75f99834d221a5fd9'}), //ракета

      this.goodsItemService.getGoodsItemByID({
        category: 'papercraft',
        subcategory: 'paper',
        id: '64a270f75f99834d221a5fdb'}), //пиратский корабль

      this.goodsItemService.getGoodsItemByID({
        category: 'papercraft',
        subcategory: 'paper',
        id: '64a270f75f99834d221a5fdf'}), //пастель

      this.goodsItemService.getGoodsItemByID({
        category: 'art',
        subcategory: 'artsupply',
        id: '64b2674ba1868fb92c1e68d9'}), //мелки звездочки

      this.goodsItemService.getGoodsItemByID({
        category: 'papercraft',
        subcategory: 'paper',
        id: '64a270f75f99834d221a5fe1'}), //бумага пастель

      this.goodsItemService.getGoodsItemByID({
        category: 'art',
        subcategory: 'artsupply',
        id: '64b2674ba1868fb92c1e68d3'}), //смываемые фломастеры

        this.goodsItemService.getGoodsItemByID({
          category: 'papercraft',
          subcategory: 'paper',
          id: '64a270f75f99834d221a5fe7'}) //бумага солнечный
    ]).subscribe((data:any)=>{
      console.log('получены товары для домашней карусели ', data);
      const goodsArr = data;
      const goodsArrFlat = goodsArr.flat()
      console.log('вывести общий массив товаров для карусели выбор ', goodsArrFlat);

       this.bestChoiceGoods = this.goodsListPageService.countGoodsTotalRating(goodsArrFlat);
       console.log('вывести новый массив с рейтингом ', this.bestChoiceGoods);

       this.bestChoiceGoodsColl = this.bestChoiceGoods.map((item: any)=>{
         const catObj = this.goodsCategoriesCollection.find((cat: any) => item.subcategory === cat.subcategory);
         console.log('вывести найденный объект категории ', catObj);

         //добавляем новые поля в объект товара
         item.categoryUrl = catObj.categoryUrl;
         item.subcategoryUrl = catObj.subcategoryUrl;
          return item;
       });
       console.log('вывести массив с путями ', this.bestChoiceGoodsColl);
    });


  }

  goToGoodsCard(choice:IGoodsCard){
    const id = choice.product_number
    console.log ('передали объект товара при клике по img ', choice);
    this.router.navigate([`papercraft/${choice.product_number}`]);
}

onBuyButtonClick(id:string){
    if(id==="summer"){
      this.router.navigate(['/handmade','feature','summer']);
    }
    if(id==="party"){
      this.router.navigate(['/handmade','occasion','partysupply']);
    }
}

ngOnDestroy() {
    this.carouselOffersCollection?.unsubscribe();
}
}
