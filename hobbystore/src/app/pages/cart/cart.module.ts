import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { TableModule } from 'primeng/table';
import { CartRoutingModule } from './cart-routing.module';
import {CartComponent} from "./cart.component";
import { CheckboxModule } from 'primeng/checkbox';
import {ReactiveFormsModule} from "@angular/forms";
import { RadioButtonModule } from 'primeng/radiobutton';
import { FormsModule } from '@angular/forms';
import {ToastModule} from 'primeng/toast';
import {MessageService} from "primeng/api";




@NgModule({
  declarations: [
    CartComponent
  ],
  imports: [
    CommonModule,
    BreadcrumbModule,
    TableModule,
    CheckboxModule,
    ReactiveFormsModule,
    RadioButtonModule,
    FormsModule,
    ToastModule,
    CartRoutingModule
  ],
  providers: [MessageService]
})
export class CartModule { }
