import {Component, OnDestroy, OnInit} from '@angular/core';
import {MenuItem, MessageService} from "primeng/api";
import {ICart, ICartItem } from "../../models/cart/cart-item";
import {CartService} from "../../services/cart/cart.service";
import {GoodsItemService} from "../../services/goods/goods-item.service";
import {forkJoin, Observable, Subscription} from "rxjs";
import {IGoodsItemCard} from "../../models/goods/goods-item-card";
import {IGoodsItemStock} from "../../models/goods/goods-item-stock";
import { FormControl, FormGroup } from '@angular/forms';
import {Router} from "@angular/router";
import {OrderPageService} from "../../services/order/order-page.service";

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit, OnDestroy{

  items: MenuItem[] =[];
  home: MenuItem ={};

  cartUID: string | null = '';

  cartObj: ICart | undefined;

  cartArr: ICartItem[] | undefined;

  cartTotal: any[] = [];

  totalOrder: number = 0;

  goodsItemRequestArr: Observable<IGoodsItemCard>[] = [];

  goodsItemStockRequestArr: Observable<IGoodsItemStock>[] = [];

  goodsItemArr: IGoodsItemCard[] | undefined;
  cartItems!:any;
  cartItemsWithStock!: any;

  goodsItemStockArr: IGoodsItemStock[] | undefined;

  deliveryOptions: any[] = [
      { name: 'Стандартная доставка', key: 'SD',
        description: 'Почта России. Сроки доставки: 3-5 дней.',
        price: 350,
        currency: 'руб.' },
      { name: 'Экспресс-доставка', key: 'ED',
        description: 'Курьер. Сроки доставки: 1 день.',
        price: 700,
        currency: 'руб.' },
      { name: 'Бесплатная доставка', key: 'FREE',
        description: 'Бесплатная доставка при сумме заказа от 5000 руб. Сроки доставки: 3-5 дней.',
        price: 0,
        currency: 'Бесплатно' },
     ]


  constructor(private cartService: CartService,
              private goodsItemService: GoodsItemService,
              private messageService: MessageService,
              private orderPageService: OrderPageService,
              private router: Router) {
  }

  formGroup = new FormGroup({
    selectedCategory: new FormControl()
  });


  ngOnInit() {
    this.items = [{
      label: "Корзина",
       }];
     this.home = {icon: 'pi pi-home', routerLink: '/'};

    this.formGroup.setValue({selectedCategory: this.deliveryOptions[0]});
    console.log(this.formGroup.value);



     this.cartUID = this.cartService.uniqueID|| localStorage.getItem('UID');

     console.log('Вывести значения радио ', this.formGroup.value);

    if(this.cartUID){
       this.cartService.getCartByUID(this.cartUID).subscribe((data)=>{
         this.cartObj = data;
         console.log('Получили корзину с сервера ', this.cartObj);

         this.cartArr =[...this.cartObj.cart];
         console.log ('Записали массив товаров корзины ', this.cartArr);

         this.goodsItemRequestArr = this.cartArr.map((goodsItemObj)=>{
           const requestObj = {
             id: goodsItemObj.itemID,
             category: goodsItemObj.category,
             subcategory: goodsItemObj.subcategory
           };
           console.log('Объект для запроса на сервер ', requestObj);
           return this.goodsItemService.getGoodsItemByID(requestObj);
         });

         this. goodsItemStockRequestArr = this.cartArr.map((goodsItemObj)=>{
           return this.goodsItemService.getGoodsStockByItemID({subcategory: goodsItemObj.subcategory,
             id: goodsItemObj.itemID});
         });

         console.log('Массив запросов по товарам ', this.goodsItemRequestArr);
         if(this.goodsItemRequestArr){
           forkJoin(this.goodsItemRequestArr).subscribe((data)=> {
              console.log('Получили данные по товарам в корзине ', data);
              this.goodsItemArr = data;
              this.cartItems = this.goodsItemArr?.map((goodsItem)=>{
                const cartItemObj = this.cartArr?.find((cartItem)=>cartItem.itemID === goodsItem._id);
                return {itemID: goodsItem._id,
                        title: goodsItem.title,
                        img: goodsItem.img,
                        price: goodsItem.price,
                        product_number: goodsItem.product_number,
                        category: cartItemObj?.category,
                        subcategory: cartItemObj?.subcategory,
                        quantity: cartItemObj?.quantity}
              });
              console.log(this.cartItems);

            });

           if(this.goodsItemStockRequestArr){
             forkJoin(this.goodsItemStockRequestArr).subscribe((data)=>{
              this.goodsItemStockArr = data;
               console.log('Получили данные по стоку ', this.goodsItemStockArr);
               this.cartItemsWithStock = this.cartItems.map((item:any)=>{
                 const cartItemStockObj = this.goodsItemStockArr!.find((itemStock)=>itemStock.itemId === item.itemID);
                 item.stock = cartItemStockObj!.stock;
                 return item;
               });
               console.log('вывели массив корзины со стоком ', this.cartItemsWithStock);
               this.countTotalOderSum();
               console.log ('Сумма заказа ', this.totalOrder);
               this.setSelectedCategory(); //выбор типа доставки при загрузке корзины
               this.totalCartInfo();  //при загрузке корзины
               console.log("Получили объект для отображения в секции total ",this.cartTotal);
             });
           }
         }
     });

     }
 }

 countTotalOderSum(){
   this.totalOrder = this.cartItemsWithStock.reduce((sum: number, obj: any) => {
     return sum + obj.price * obj.quantity;
   }, 0);
 }
 setSelectedCategory(){
   if(this.totalOrder>=5000){
      this.formGroup.setValue({selectedCategory: this.deliveryOptions[2]});
      console.log(this.formGroup.value);
    }else {
      this.formGroup.setValue({selectedCategory: this.deliveryOptions[0]});
      console.log(this.formGroup.value);
      console.log (this.formGroup.get('selectedCategory'));
    }
 }

  decreaseQuantity(product:any){
    console.log('вывели объект товара из шаблона ', product);
    let quantityValue = product.quantity;
    if(quantityValue){
      console.log('текущее значение количества при минус ', quantityValue);
      quantityValue--;
      console.log('уменьшили количество товара ', quantityValue);

      if(quantityValue == 1){
       this.setMinusButtonState(product); //блокируем кнопку минус

       const itemObjNew = this.cartService.findGoodsItem(this.cartItemsWithStock, product);
       itemObjNew.quantity = quantityValue; //изменяем количество в массиве товаров
       console.log('вывели измененный объект товара ', itemObjNew);

        const itemIndex = this.cartService.findGoodsItemIndex(this.cartItemsWithStock,product);
        console.log('вывели индекс найденного объекта товара ', itemIndex);

        this.cartItemsWithStock.splice(itemIndex,1,itemObjNew);
        console.log('Вывели массив корзины ', this.cartItemsWithStock);

      }else{
        const itemObjNew = this.cartService.findGoodsItem(this.cartItemsWithStock,product);
        itemObjNew.quantity = quantityValue;
        console.log('вывели измененный объект товара ', itemObjNew);

        const itemIndex = this.cartService.findGoodsItemIndex(this.cartItemsWithStock,product);
        console.log('вывели индекс найденного объекта товара ', itemIndex);

        this.cartItemsWithStock.splice(itemIndex,1,itemObjNew);
        console.log('Вывели массив корзины ', this.cartItemsWithStock);
      }
       this.setPlusButtonState(product); //изменение состояния кнопки плюс
       this.countTotalOderSum();
       this.setSelectedCategory();
       this.totalCartInfo();
    }
  }

  increaseQuantity(product:any){
    console.log('вывели объект товара из шаблона ', product);
    let quantityValue = product.quantity;
    if(quantityValue) {
      console.log('текущее значение количества при плюс ', quantityValue);
      quantityValue++;

/*      if(quantityValue > this.cartItems1.stock){
          this.setPlusButtonState(product);
        }*/

        if(quantityValue == this.cartItemsWithStock.stock){
          this.setPlusButtonState(product); //блокировка кнопки плюс

          const itemObjNew = this.cartService.findGoodsItem(this.cartItemsWithStock,product);
          itemObjNew.quantity = quantityValue;
          console.log('вывели измененный объект товара ', itemObjNew);

          const itemIndex = this.cartService.findGoodsItemIndex(this.cartItemsWithStock,product);
          console.log('вывели индекс найденного объекта товара ', itemIndex);

          this.cartItemsWithStock.splice(itemIndex,1,itemObjNew);
          console.log('Вывели массив корзины ', this.cartItemsWithStock);
      }else {
          const itemObjNew = this.cartService.findGoodsItem(this.cartItemsWithStock,product);
          itemObjNew.quantity = quantityValue;
          console.log('вывели измененный объект товара ', itemObjNew);

          const itemIndex = this.cartService.findGoodsItemIndex(this.cartItemsWithStock,product);
          console.log('вывели индекс найденного объекта товара ', itemIndex);

          this.cartItemsWithStock.splice(itemIndex,1,itemObjNew);
          console.log('Вывели массив корзины ', this.cartItemsWithStock);
       }
      this.setMinusButtonState(product); //изменение состояния кнопки минус
      this.countTotalOderSum();
      this.setSelectedCategory();
      this.totalCartInfo();
    }
  }

 setMinusButtonState(product:any): boolean{
    return product.quantity == 1;
  }

  setPlusButtonState(product:any): boolean{
    return product.quantity >= product.stock;
  }

  getSelectedCategory(ev:any){
    console.log(ev);
    const radioValue = this.formGroup.value;
    console.log(radioValue.selectedCategory!.key);
    console.log(radioValue.selectedCategory);
  }

  ngOnDestroy() {
    // this.valueChangesSubscription?.unsubscribe();
  }

  totalCartInfo(){
    this.cartTotal = [];
    const radioValue = this.formGroup.value;
    if(radioValue.selectedCategory) {
      if (radioValue.selectedCategory.key === "FREE" && this.totalOrder < 5000) {
        const totalOderInfo = {
          order: this.totalOrder,
          delivery_type: this.deliveryOptions[0].name,
          delivery_price: this.deliveryOptions[0].price,
          total: this.totalOrder + radioValue.selectedCategory.price
        }
        this.messageService.add({ severity: 'warn', summary: 'Бесплатная доставка от 5000 руб.'});
        console.log('получили объект заказа ',totalOderInfo);
        this.cartTotal.push(totalOderInfo);
      } else {
        const totalOderInfo = {
          order: this.totalOrder,
          delivery_type: radioValue.selectedCategory.name,
          delivery_price: radioValue.selectedCategory.price,
          total: this.totalOrder + radioValue.selectedCategory.price
        }

        console.log('получили объект заказа ',totalOderInfo);
        this.cartTotal.push(totalOderInfo);
      }
    }
  }

  removeItemFromCart(product:any){
    console.log('вывели объект товара из шаблона ', product);

    const itemIndex = this.cartService.findGoodsItemIndex(this.cartItemsWithStock,product);
    console.log('вывели индекс найденного объекта товара ', itemIndex);

    this.cartItemsWithStock.splice(itemIndex,1);
    console.log('Вывели массив корзины ', this.cartItemsWithStock);
    this.countTotalOderSum();
    this.setSelectedCategory();
    this.totalCartInfo();
  }

  checkOut(){
    const oderObj = {
      orderInfo: this.cartTotal,
      goodsItems: this.cartItemsWithStock
    }
    this.orderPageService.saveOderInfo(oderObj);
    this.router.navigate(['/handmade/checkout']);
  }
 }







