
//используется в landing component для сохранения пункта меню по клику
export interface IMenuCategory{
  category: string,
  subcategory: string
}

export interface IMenuCategoryInfo extends IMenuCategory{
  categoryUrl: string;
  subcategoryUrl: string;
  description: string
}
