//используется в окне поиска для вывода популярных категорий и популярных поисков
export interface ICategories{
  name: string;
  rating: number;
}
