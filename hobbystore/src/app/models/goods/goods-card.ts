export interface IGoodsCard{
  title: string;
  price: number;
  rating: number;
  group: string;
  img: string;
  product_number: string;
}
