export interface IGoodsItemStock{
  itemId: string;
  stock: number;
  reserve: number;
}
