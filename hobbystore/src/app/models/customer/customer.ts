export interface ICustomer{
  firstName: string|null,
  secondName?: string|null,
  lastName : string|null,
  mobile: string|null,
  index?: string|null
  region: string|null,
  city: string|null,
  address: string|null
}
