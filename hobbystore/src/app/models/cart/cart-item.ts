export interface ICartItem {
  itemID: string;
  quantity: number;
  category: string;
  subcategory: string;
}
export interface ICartItemUID extends ICartItem {
  UID: string;
}

export interface ICart {
  UID: string;
  cart: ICartItem[];
}

export interface ICartObject{
  itemID: string|undefined,
  title: string,
  img: string,
  price: number,
  product_number: string,
  category: string;
  subcategory: string;
  quantity: any;
}
