export interface IOderGoodsItem {
  itemID: string;
  title: string;
  img: string;
  product_number: string;
  quantity: number;
  category: string;
  subcategory: string;
  price: number;
  stock: number;
}

export interface IOderInfo {
  order: number;
  delivery_type: string;
  delivery_price: number;
  total: number;
  orderNumber: number;
}

export interface IOrder {
  firstName: string;
  secondName?: string;
  lastName: string;
  mobile: string;
  index?: string;
  region: string;
  city: string;
  address: string;
  orderNumber: number;
  goodsItems: IOderGoodsItem[];
  orderInfo: IOderInfo[];
}
