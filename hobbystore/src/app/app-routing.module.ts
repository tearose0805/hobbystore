import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./pages/home/home.component";
import {OrderModule} from "./pages/order/order.module";

const routes: Routes = [
  {
    path: 'handmade/checkout',
    loadChildren: ()  => import('./pages/order/order.module').then(m => OrderModule)
  },
 {
    path: 'handmade',
    loadChildren: ()  => import('./pages/landing-page/landing-page.module').then(m => m.LandingPageModule),
  },
  // { path: 'goods-card', loadChildren: () => import('./pages/goods/goods-card/goods-card.module').then(m => m.GoodsCardModule) },
  { path: '**',
    redirectTo: 'handmade'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{anchorScrolling:"enabled"}),
    ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
